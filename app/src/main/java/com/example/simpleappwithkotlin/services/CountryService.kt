package com.example.simpleappwithkotlin.services

import com.example.simpleappwithkotlin.models.MyCountry
import retrofit2.Call
import retrofit2.http.GET

interface CountryService {

    @GET("countries")
    fun getAffectedCountryList () : Call<List<MyCountry>>
}